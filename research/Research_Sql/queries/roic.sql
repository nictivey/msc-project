SELECT t0.ticker, t1.name, t0.price as before, t2.price as after, t0.roic, RANK () OVER ( ORDER BY t0.roic DESC) AS roic_rank, (100*((t3.price - t0.price)/t0.price)) AS price_growth
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1, market_data.Fundamentals as t2
WHERE t0.calendardate = '2018-03-31'
AND t0.permaticker = t1.permaticker
AND t0.permaticker = t2.permaticker
AND t0.price IS NOT null
AND t0.roic IS NOT null
AND t0.roic >= 0
AND t0.dimension = 'ART'
AND t2.dimension = 'ART'
ORDER BY roic_rank
LIMIT 20;