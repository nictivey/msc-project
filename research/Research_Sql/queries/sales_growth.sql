SELECT t0.ticker, t1.name, t0.price AS 'before', t3.price AS after, t2.revenue AS 'Before', t0.revenue AS 'After', (((t0.revenue - t2.revenue)/t2.revenue)*100) AS revenue_growth, RANK () OVER ( ORDER BY (((t0.revenue - t2.revenue)/t2.revenue)*100) DESC) AS revenue_growth_rank, (100*((t3.price - t0.price)/t0.price)) AS price_growth
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1, market_data.Fundamentals as t2, market_data.Fundamentals as t3
WHERE t0.calendardate = '2018-03-31'
  AND t2.calendardate = '2008-03-31'
  AND t3.calendardate = '2019-03-31'
  AND t0.permaticker = t1.permaticker
  AND t0.permaticker = t2.permaticker
  AND t0.permaticker = t3.permaticker
  AND t0.price IS NOT null
  AND t0.revenue IS NOT null
  AND t0.revenue >= 0
  AND t0.dimension = 'ARQ'
  AND t2.dimension = 'ARQ'
  AND t3.dimension = 'ARQ'
ORDER BY revenue_growth_rank
LIMIT 23;