SELECT t0.ticker, t1.name, t0.price, (t0.liabilities/t0.ncfo) AS dof, RANK () OVER ( ORDER BY (t0.liabilities/t0.ncfo) ASC) AS dof_rank
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1
WHERE t0.calendardate = '2018-03-31'
AND t0.permaticker = t1.permaticker 
AND (t0.liabilities/t0.ncfo) IS NOT null
AND t0.price IS NOT null
AND (t0.liabilities/t0.ncfo) >= 0
AND t0.dimension = 'ARQ'
ORDER BY dof_rank
LIMIT 20;