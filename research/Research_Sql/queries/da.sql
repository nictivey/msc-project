SELECT t0.ticker, t1.name, t0.price AS 'before', t2.price AS after, (t0.liabilities/t0.assets) as da, RANK () OVER ( ORDER BY (t0.liabilities/t0.assets) ASC) AS da_rank, (100*((t2.price - t0.price)/t0.price)) AS price_growth
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1, market_data.Fundamentals as t2
WHERE t0.calendardate = '2018-03-31'
  AND t2.calendardate = '2019-03-31'
  AND t0.permaticker = t1.permaticker
  AND t0.permaticker = t2.permaticker
  AND t0.price IS NOT null
  AND (t0.liabilities/t0.assets) IS NOT null
  AND (t0.liabilities/t0.assets) >= 0
  AND t0.dimension = 'ARQ'
  AND t2.dimension = 'ARQ'
ORDER BY da_rank
LIMIT 23;
