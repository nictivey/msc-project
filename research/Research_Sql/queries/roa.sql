SELECT t0.ticker, t1.name, t0.price, t0.roa, RANK () OVER ( ORDER BY t0.roa DESC) AS roa_rank
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1
WHERE t0.calendardate = '2018-03-31'
AND t0.permaticker = t1.permaticker
AND t0.price IS NOT null
AND t0.roa IS NOT null
AND t0.roa >= 0
AND t0.dimension = 'ART'
ORDER BY roa_rank
LIMIT 20;