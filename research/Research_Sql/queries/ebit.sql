SELECT t0.ticker, t1.name, t0.price, t0.ebit, RANK () OVER ( ORDER BY t0.ebit DESC) AS ebit_rank
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1
WHERE t0.calendardate = '2018-03-31'
AND t0.ebit IS NOT null
AND t0.ebit >= 0
AND t0.permaticker = t1.permaticker
AND t0.dimension = 'ARQ'
ORDER BY ebit_rank
LIMIT 20;