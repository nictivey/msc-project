SELECT t0.ticker, t1.name, t0.price AS price_2018, t3.price AS price_2019,
       (((t0.revenue - t2.revenue)/t2.revenue)*100) AS revenue_growth,
       RANK () OVER ( ORDER BY (((t0.revenue - t2.revenue)/t2.revenue)*100) DESC) AS revenue_growth_rank,
       t0.gp, RANK () OVER ( ORDER BY t0.gp DESC) AS gp_rank,
       t0.de, RANK () OVER ( ORDER BY t0.de ASC) AS de_rank,
       t0.currentratio, RANK () OVER ( ORDER BY t0.currentratio DESC) AS currentratio_rank,
       t0.eps, RANK () OVER ( ORDER BY t0.eps DESC) AS eps_rank,
       ((15/65) *  (RANK () OVER ( ORDER BY (((t0.revenue - t2.revenue)/t2.revenue)*100) DESC)) +
        (14/65) * (RANK () OVER ( ORDER BY t0.gp DESC)) + 0.2 * (RANK () OVER ( ORDER BY t0.de ASC)) +
        (12/65) * (RANK () OVER ( ORDER BY t0.currentratio DESC)) +
        (11/65) * (RANK () OVER ( ORDER BY t0.eps DESC))) AS aggregated, (100*((t3.price - t0.price)/t0.price)) AS price_growth
FROM market_data.Fundamentals AS t0,
     market_data.Tickers AS t1,
     market_data.Fundamentals AS t2,
     market_data.Fundamentals AS t3
WHERE t0.calendardate = '2018-03-31'
  AND t2.calendardate = '2008-03-31'
  AND t3.calendardate = '2019-03-31'
  AND t0.de IS NOT null
  AND t0.de >= 0
  AND t0.gp IS NOT null
  AND t0.gp >= 0
  AND t0.currentratio IS NOT null
  AND t0.currentratio >= 0
  AND t0.eps IS NOT null
  AND t0.eps >= 0
  AND t0.revenue IS NOT null
  AND t0.revenue >= 0
  AND t0.permaticker = t1.permaticker
  AND t0.permaticker = t2.permaticker
  AND t0.permaticker = t3.permaticker
  AND t0.dimension = 'ARQ'
  AND t2.dimension = 'ARQ'
  AND t3.dimension = 'ARQ'
ORDER BY aggregated
LIMIT 21;