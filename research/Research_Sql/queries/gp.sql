SELECT t0.ticker, t1.name, t0.price, t0.gp, RANK () OVER ( ORDER BY t0.gp ASC) AS gp_rank
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1
WHERE t0.calendardate = '2019-03-31'
AND t0.de IS NOT null
AND t0.price IS NOT null
AND t0.de >= 0
AND t0.permaticker = t1.permaticker
AND t0.dimension = 'ARQ'
ORDER BY gp_rank
LIMIT 20;