SELECT t0.ticker, t1.name, t0.price, t0.eps, RANK () OVER ( ORDER BY t0.eps DESC) AS eps_rank
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1
WHERE t0.calendardate = '2018-03-31'
AND t0.eps IS NOT null
AND t0.eps >= 0
AND t0.permaticker = t1.permaticker
AND t0.dimension = 'ARQ'
ORDER BY eps_rank
LIMIT 20;