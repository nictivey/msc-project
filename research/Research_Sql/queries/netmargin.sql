SELECT t0.ticker, t1.name, t0.price, t0.netmargin, RANK () OVER ( ORDER BY t0.netmargin DESC) AS netmargin_rank
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1
WHERE t0.calendardate = '2018-03-31'
AND t0.permaticker = t1.permaticker
AND t0.price IS NOT null
AND t0.netmargin IS NOT null
AND t0.netmargin >= 0
AND t0.dimension = 'ARQ'
ORDER BY netmargin_rank
LIMIT 20;