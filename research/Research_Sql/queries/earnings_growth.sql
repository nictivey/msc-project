SELECT t0.ticker, t1.name, t0.price, t2.ebit AS 'Before', t0.ebit AS 'After', (((t0.ebit - t2.ebit)/t2.ebit)*100) AS ebit_growth, RANK () OVER ( ORDER BY (((t0.ebit - t2.ebit)/t2.ebit)*100) DESC) AS earnings_growth_rank
FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1, market_data.Fundamentals AS t2
WHERE t0.calendardate = '2018-03-31'
AND t2.calendardate = '2008-03-31'
AND t0.permaticker = t1.permaticker
AND t0.permaticker = t2.permaticker
AND t0.ebit IS NOT null
AND t0.ebit >= 0
AND t0.dimension = 'ARQ'
AND t2.dimension = 'ARQ'
ORDER BY earnings_growth_rank
LIMIT 20;