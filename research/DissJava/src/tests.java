import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class tests{

        /** @author
         *  <Nicholas Tivey>
         * This class contains the test cases for Best First Informed Search Algorithm Methods.
         */

    //Getters for variable class tests
    @Test
    public void test1(){

        variable random = new variable("tester", 10.0, "asc", "extra");

        String expectedName = "tester";
        double expectedValue = 10.0;
        String expectedOrder = "asc";
        String expectedExtra = "extra";

        String actualName = random.getName();
        double actualValue = random.getValue();
        String actualOrder = random.getOrder();
        String actualExtra = random.getExtra();

        assertEquals(expectedName, actualName);
        assertEquals(expectedValue, actualValue);
        assertEquals(expectedOrder, actualOrder);
        assertEquals(expectedExtra, actualExtra);
    }

    //Strength Getters for node class tests

    //Expexted Data
    @Test
    public void test2(){
        variable v1 = new variable("v1", 10.0, "asc", "");
        variable v2 = new variable("v2", 20.0, "asc", "");
        variable v3 = new variable("v3", -10.0, "desc", "");

        node n1 = new node(v1, v2, v3);

        double expectedScore = 0;
        double actualScore = n1.getScore();

        double expectedStrength1 = 10.0/20.0;
        double actualStrength1 = n1.getStr1();

        double expectedStrength2 = 20.0/20.0;
        double actualStrength2 = n1.getStr2();

        double expectedStrength3 = -10.0/20.0;
        double actualStrength3 = n1.getStr3();

        assertEquals(expectedScore, actualScore);
        assertEquals(expectedStrength1, actualStrength1);
        assertEquals(expectedStrength2, actualStrength2);
        assertEquals(expectedStrength3, actualStrength3);
    }

    //Boundary Data pt 1
    @Test
    public void test3(){

        variable v1 = new variable("v1", 0.0, "asc", "");
        variable v2 = new variable("v2", 10.0, "asc", "");
        variable v3 = new variable("v3", -10.0, "desc", "");

        node n1 = new node(v1, v2, v3);

        double expectedStrength1 = 0;
        double actualStrength1 = n1.getStr1();

        double expectedStrength2 = 10.0/30.0;
        double actualStrength2 = n1.getStr2();

        double expectedStrength3 = -10.0/30.0;
        double actualStrength3 = n1.getStr3();

        assertEquals(expectedStrength1, actualStrength1);
        assertEquals(expectedStrength2, actualStrength2);
        assertEquals(expectedStrength3, actualStrength3);
    }

    //Boundary Data pt 2
    @Test
    public void test4(){
        variable v1 = new variable("v1", 0.0, "asc", "");
        variable v2 = new variable("v2", 10.0, "asc", "");
        variable v3 = new variable("v3", -50.0, "desc", "");

        node n1 = new node(v1, v2, v3);

        double expectedStrength1 = 0;
        double actualStrength1 = n1.getStr1();

        double expectedStrength2 = 10.0/-40.0;
        double actualStrength2 = n1.getStr2();

        double expectedStrength3 = -10.0/30.0;
        double actualStrength3 = n1.getStr3();

        assertEquals(expectedStrength1, actualStrength1);
        assertEquals(expectedStrength2, actualStrength2);
        assertEquals(expectedStrength3, actualStrength3);
    }

    //Score Getter method for node class tests
    @Test
    public void test5(){
        variable v1 = new variable("v1", 10.0, "asc", "a");
        variable v2 = new variable("v2", 20.0, "asc", "b");
        variable v3 = new variable("v3", -10.0, "desc", "c");

        node n1 = new node(v1, v2, v3);

        double expectedScore = 0;
        double actualScore = n1.getScore();

        assertEquals(expectedScore, actualScore);
    }

    //Setters for node class tests
    @Test
    public void test6(){
        variable v1 = new variable("v1", 10.0, "asc", "");
        variable v2 = new variable("v2", 20.0, "asc", "");
        variable v3 = new variable("v3", -10.0, "desc", "");

        node n1 = new node(v1, v2, v3);
        n1.setScore(100.0);

        double expectedScore = 100.0;
        double actualScore = n1.getScore();

        assertEquals(expectedScore, actualScore);

    }

    //Tests for isNeighbour() method
    @Test
    public void test7(){
        variable v1 = new variable("v1", 0, "asc", "");
        variable v2 = new variable("v2", 0, "asc", "");
        variable v3 = new variable("v3", 0, "desc", "");
        variable v4 = new variable("v4", 0, "asc", "");
        variable v5 = new variable("v5", 0, "asc", "");
        variable v6 = new variable("v6", 0, "desc", "");


        node n1 = new node(v1, v2, v3);
        node n2 = new node(v4, v5, v6);
        node n3 = new node(v6, v4, v1);

        boolean expected1 = node.isNeighbour(n1, n2);
        boolean expected2 = node.isNeighbour(n1, n3);
        boolean expected3 = node.isNeighbour(n2, n3);

        assertFalse(expected1);
        assertFalse(expected2);
        assertTrue(expected3);
    }

    //Tests for isSame() method
    @Test
    public void test8(){
        variable v1 = new variable("v1", 0, "asc", "");
        variable v2 = new variable("v2", 0, "asc", "");
        variable v3 = new variable("v3", 0, "desc", "");
        variable v4 = new variable("v4", 0, "asc", "");


        node n1 = new node(v1, v2, v3);
        node n2 = new node(v4, v3, v2);
        node n3 = new node(v3, v1, v2);

        boolean expected1 = node.isSame(n1, n2);
        boolean expected2 = node.isSame(n1, n3);
        boolean expected3 = node.isSame(n2, n3);

        assertFalse(expected1);
        assertTrue(expected2);
        assertFalse(expected3);
    }

    //Tests for makeNodes() method (4 variables)
    @Test
    public void test9(){
        variable v1 = new variable("v1", 0, "asc", "");
        variable v2 = new variable("v2", 0, "asc", "");
        variable v3 = new variable("v3", 0, "desc", "");
        variable v4 = new variable("v4", 0, "asc", "");
        variable v5 = new variable("v5", 0, "asc", "");

        ArrayList<variable> variables = new ArrayList<>();
        variables.add(v1); variables.add(v2); variables.add(v3); variables.add(v4); variables.add(v5);
        node n1 = new node(v1, v2, v3);
        node n2 = new node(v1, v2, v4);
        node n3 = new node(v1, v3, v4);
        node n4 = new node(v2, v3, v4);
        node n5 = new node(v1, v2, v5);
        node n6 = new node(v1, v3, v5);
        node n7 = new node(v2, v3, v5);
        node n8 = new node(v3, v4, v5);
        node n9 = new node(v1, v4, v5);
        node n10 = new node(v2, v4, v5);

        ArrayList<node> expectedNodes = new ArrayList<>();
        expectedNodes.add(n1); expectedNodes.add(n2); expectedNodes.add(n3); expectedNodes.add(n4);
        expectedNodes.add(n5); expectedNodes.add(n6); expectedNodes.add(n7); expectedNodes.add(n8);
        expectedNodes.add(n9); expectedNodes.add(n10);

        ArrayList<node> actualNodes = node.makeNodes(variables);

        assertEquals(expectedNodes.size(), actualNodes.size());
    }

    //Tests for makeNodes() method (16 variables)
    @Test
    public void test10(){
        variable revGrowth = new variable("t1.revenue", -70, "desc", "(100*((t1.revenue - t0.revenue)/t0.revenue))");
        variable earnGrowth = new variable("t1.ebit", -91,"desc","(100*((t1.ebit - t0.ebit)/t0.ebit))");
        variable gp = new variable("t1.gp", 216,"desc","t1.gp");
        variable ebt = new variable("t1.ebt",350 ,"desc","t1.ebt");
        variable eps = new variable("t1.eps", 170,"desc","t1.eps");
        variable ebit = new variable("t1.ebit", 277,"desc","t1.ebit");
        variable netmargin = new variable("t1.netmargin", 27,"desc","t1.netmargin");
        variable roa = new variable("t2.roa", -265,"desc","t2.roa");
        variable roe = new variable("t2.roe", -446,"desc","t2.roe");
        variable roic = new variable("t2.roic", -108,"desc","t2.roic");
        variable fcf = new variable("t1.fcf", 173,"desc","t1.fcf");
        variable de = new variable("t1.liabilities/t1.equity",191 ,"asc","t1.liabilities/t1.equity");
        variable da = new variable("t1.liabilities/t1.assets", 292,"asc","t1.liabilities/t1.assets");
        variable df = new variable("t1.liabilities/t1.fcf",-121 ,"asc","t1.liabilities/t1.fcf");
        variable dof = new variable("t1.liabilities/t1.ncfo", -235,"asc","t1.liabilities/t1.ncfo");
        variable cr = new variable("t1.currentratio", -27,"desc","t1.currentratio");

        ArrayList<variable> variables = new ArrayList<>();
        variables.add(revGrowth); variables.add(earnGrowth); variables.add(gp); variables.add(ebt);
        variables.add(eps); variables.add(ebit); variables.add(netmargin); variables.add(roa);
        variables.add(roe); variables.add(roic); variables.add(fcf); variables.add(de);
        variables.add(da); variables.add(df); variables.add(dof); variables.add(cr);

        ArrayList<node> actualNodes = node.makeNodes(variables);

        int expectedNodesSize = (16*15*14)/6; //number of combinations where order does not matter

        assertEquals(expectedNodesSize, actualNodes.size());

    }

    //Tests for findBestNode() method
    @Test
    public void test11(){
        variable v1 = new variable("v1", 0, "asc", "");
        variable v2 = new variable("v2", 0, "asc", "");
        variable v3 = new variable("v3", 0, "desc", "");
        variable v4 = new variable("v4", 0, "asc", "");
        variable v5 = new variable("v5", 0, "asc", "");

        node n1 = new node(v1, v2, v3);
        node n2 = new node(v1, v2, v4);
        node n3 = new node(v1, v2, v5);
        node n4 = new node(v1, v3, v4);
        node n5 = new node(v1, v3, v5);
        node n6 = new node(v1, v4, v5);
        node n7 = new node(v2, v3, v4);
        node n8 = new node(v2, v3, v5);
        node n9 = new node(v2, v4, v5);
        node n10 = new node(v3, v4, v5);

        n1.setScore(200);
        n2.setScore(300);
        n3.setScore(400);
        n4.setScore(100);
        n5.setScore(155);
        n6.setScore(620);
        n7.setScore(-2000);
        n8.setScore(10);
        n9.setScore(440);
        n10.setScore(1000);

        ArrayList<node> nodes = new ArrayList<>();
        nodes.add(n1); nodes.add(n2); nodes.add(n3); nodes.add(n4); nodes.add(n5);
        nodes.add(n6); nodes.add(n7); nodes.add(n8); nodes.add(n9); nodes.add(n10);
        String actualResult = node.findBestNode(nodes.get(0), nodes, 800);
        String expectedResult = n10.toString();

        assertEquals(expectedResult, actualResult);
    }

    //Queries have been tested independently
}
