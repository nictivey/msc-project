public class variable {

    private String name;
    private double value;
    private String order;
    private String extra;

    public variable(String name, double value, String order, String extra){
        this.name = name;
        this.value = value;
        this.order = order;
        this.extra = extra;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public String getOrder(){
        return order;
    }

    public String getExtra(){
        return extra;
    }
}
