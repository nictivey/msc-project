import java.sql.ResultSet;
import java.sql.*;
import java.util.ArrayList;

public class node {

    private variable var1;
    private variable var2;
    private variable var3;
    private double str1;
    private double str2;
    private double str3;
    private double score;

    public node(variable var1, variable var2, variable var3) {
        this.var1 = var1;
        this.var2 = var2;
        this.var3 = var3;
        this.score = 0.0;
        this.str1 = getStrength1(this);
        this.str2 = getStrength2(this);
        this.str3 = getStrength3(this);
    }

    public static double getStrength1(node node) {

        double strength;
        double pairWeight = node.getVar1().getValue();
        double sumOfWeights = node.getVar1().getValue() + node.getVar2().getValue() + node.getVar3().getValue();
        if (pairWeight == 0){
            strength = 0;
        } else if (sumOfWeights == 0){
            strength = pairWeight / (Math.abs(pairWeight) * 3.0);
        } else if (pairWeight > 0 && sumOfWeights > 0) {
            strength = pairWeight / sumOfWeights;
        } else if (pairWeight > 0 && sumOfWeights < 0){
            strength = - (pairWeight / sumOfWeights);
        } else if (sumOfWeights > 0 && pairWeight < 0){
            strength = (pairWeight / sumOfWeights);
        } else {
            strength = -(pairWeight / sumOfWeights);
        }
        return strength;
    }

    public static double getStrength2(node node) {
        double strength;
        double pairWeight = node.getVar2().getValue();
        double sumOfWeights = node.getVar1().getValue() + node.getVar2().getValue() + node.getVar3().getValue();
        if (pairWeight == 0){
            strength = 0;
        } else if (sumOfWeights == 0){
            strength = pairWeight / (Math.abs(pairWeight) * 3.0);
        } else if (pairWeight > 0 && sumOfWeights > 0) {
            strength = pairWeight / sumOfWeights;
        } else if (pairWeight > 0 && sumOfWeights < 0){
            strength = - (pairWeight / sumOfWeights);
        } else if (sumOfWeights > 0 && pairWeight < 0){
            strength = (pairWeight / sumOfWeights);
        } else {
            strength = -(pairWeight / sumOfWeights);
        }
        return strength;
    }

    public static double getStrength3(node node) {
        double strength;
        double pairWeight = node.getVar3().getValue();
        double sumOfWeights = node.getVar1().getValue() + node.getVar2().getValue() + node.getVar3().getValue();
        if (pairWeight == 0){
            strength = 0;
        } else if (sumOfWeights == 0){
            strength = pairWeight / (Math.abs(pairWeight) * 3.0);
        } else if (pairWeight > 0 && sumOfWeights > 0) {
            strength = pairWeight / sumOfWeights;
        } else if (pairWeight > 0 && sumOfWeights < 0){
            strength = - (pairWeight / sumOfWeights);
        } else if (sumOfWeights > 0 && pairWeight < 0){
            strength = (pairWeight / sumOfWeights);
        } else {
            strength = -(pairWeight / sumOfWeights);
        }
        return strength;
    }

    public static boolean isNeighbour(node currentNode, node otherNode){
        int noSame = 0;
        if (currentNode.getVar1() == otherNode.getVar1()){
            noSame++;
        } else if (currentNode.getVar1() == otherNode.getVar2()){
            noSame++;
        } else if (currentNode.getVar1() == otherNode.getVar3()){
            noSame++;
        } if (currentNode.getVar2() == otherNode.getVar1()){
            noSame++;
        } else if (currentNode.getVar2() == otherNode.getVar2()){
            noSame++;
        } else if (currentNode.getVar2() == otherNode.getVar3()){
            noSame++;
        } if (currentNode.getVar3() == otherNode.getVar1()){
            noSame++;
        } else if (currentNode.getVar3() == otherNode.getVar2()){
            noSame++;
        } else if (currentNode.getVar3() == otherNode.getVar3()){
            noSame++;
        }
        if (noSame > 1){
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSame(node n1, node n2){
        int noSame = 0;
        if (n1.getVar1() == n2.getVar1()){
            noSame++;
        } else if (n1.getVar1() == n2.getVar2()){
            noSame++;
        } else if (n1.getVar1() == n2.getVar3()){
            noSame++;
        } if (n1.getVar2() == n2.getVar1()){
            noSame++;
        } else if (n1.getVar2() == n2.getVar2()){
            noSame++;
        } else if (n1.getVar2() == n2.getVar3()){
            noSame++;
        } if (n1.getVar3() == n2.getVar1()){
            noSame++;
        } else if (n1.getVar3() == n2.getVar2()){
            noSame++;
        } else if (n1.getVar3() == n2.getVar3()){
            noSame++;
        }
        if (noSame > 2){
            return true;
        } else {
            return false;
        }
    }

    public static ArrayList<node> makeNodes(ArrayList<variable> variables) {
        ArrayList<node> results = new ArrayList<>();
        for (int i = 0; i < variables.size(); i++) {
            for (int j = 0; j < variables.size(); j++) {
                for (int k = 0; k < variables.size(); k++) {
                    if (!(variables.get(i).getExtra() == variables.get(j).getExtra() ||
                            variables.get(i).getExtra() == variables.get(k).getExtra() ||
                            variables.get(j).getExtra() == variables.get(k).getExtra())) {

                        node currentNode1 = new node(variables.get(i), variables.get(j), variables.get(k));

                        if (results.size() == 0){
                            results.add(currentNode1);
                        } else {
                            boolean add = true;
                            for (int x = 0; x < results.size(); x++) {
                                if (isSame(results.get(x), currentNode1)) {
                                    add = false;
                                }
                            }
                            if (add) {
                                results.add(currentNode1);
                            }
                        }

                    }
                }
            }
        }
        return results;
    }

    public static String makeQuery(node node){
        String query = "";
        String var1 = node.getVar1().getName();
        String var2 = node.getVar2().getName();
        String var3 = node.getVar3().getName();

        double strength1 = node.getStr1();
        double strength2 = node.getStr2();
        double strength3 = node.getStr3();

        String order1 = node.getVar1().getOrder();
        String order2 = node.getVar2().getOrder();
        String order3 = node.getVar3().getOrder();

        String extra1 = node.getVar1().getExtra();
        String extra2 = node.getVar2().getExtra();
        String extra3 = node.getVar3().getExtra();

        String oldPeriod1 = "";
        String oldPeriod2 = "";
        String oldPeriod3 = "";
        String oldPeriod4 = "";

        if (extra1 == "(100*((t1.revenue - t0.revenue)/t0.revenue))" || extra1 == "(100*((t1.ebit - t0.ebit)/t0.ebit))" ||
                extra2 == "(100*((t1.revenue - t0.revenue)/t0.revenue))" || extra2 == "(100*((t1.ebit - t0.ebit)/t0.ebit))" ||
                extra3 == "(100*((t1.revenue - t0.revenue)/t0.revenue))" || extra3 == "(100*((t1.ebit - t0.ebit)/t0.ebit))"){
            oldPeriod1 = ", market_data.Fundamentals AS t0 ";
            oldPeriod2 = "AND t0.calendardate = '2008-03-31' ";
            oldPeriod3 = "AND t1.permaticker = t0.permaticker ";
            oldPeriod4 = "AND t0.dimension = 'ARQ' ";
        }

        query = "SELECT SUM(top20.price_growth) AS score " +
                "FROM (" +
                "SELECT (100*((t4.price - t1.price)/t1.price)) AS price_growth " +
                "FROM market_data.Fundamentals AS t1, market_data.Fundamentals AS t2, " +
                "market_data.Fundamentals AS t4 " + oldPeriod1 +
                "WHERE t1.calendardate = '2018-03-31' " +
                "AND t2.calendardate = '2018-03-31' " +
                "AND t4.calendardate = '2019-03-31' " +
                oldPeriod2 +
                "AND " + var1 + " IS NOT null " +
                "AND " + var1 + " > 0 " +
                "AND " + var2 + " IS NOT null " +
                "AND " + var2 + " > 0 " +
                "AND " + var3 + " IS NOT null " +
                "AND " + var3 + " > 0 " +
                oldPeriod3 +
                "AND t1.permaticker = t2.permaticker " +
                "AND t1.permaticker = t4.permaticker " +
                oldPeriod4 +
                "AND t1.dimension = 'ARQ' " +
                "AND t2.dimension = 'ART' " +
                "AND t4.dimension = 'ARQ' " +
                "ORDER BY ((1 - " + strength1 + ")*(RANK () OVER ( ORDER BY " + extra1 + " " + order1 + ")) + " +
                         "(1 - " + strength2 + ")*(RANK () OVER ( ORDER BY " + extra2 + " " + order2 + ")) + " +
                         "(1 - " + strength3 + ")*(RANK () OVER ( ORDER BY " + extra3 + " " + order3 + "))) ASC " +
                "LIMIT 20) AS top20;";

        return query;
    }

    public static String findBestNode(node startingNode, ArrayList<node> nodes, double target) {
        double bestScore = 0;
        node currentNode = startingNode;
        node bestNode = startingNode;
        try {
            // create our mysql database connection
            String myUrl = "jdbc:mysql://localhost:3306/market_data?useUnicode=true^useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            Connection conn = DriverManager.getConnection(myUrl, "root", "Smokey123$");
            Statement st = conn.createStatement();

            if (startingNode.getScore() == 0) {

                String query = makeQuery(startingNode);
                ResultSet rs = st.executeQuery(query);
                while (rs.next()) {
                    bestScore = rs.getInt("score");
                    startingNode.setScore(bestScore);
                }
            }
            ArrayList<node> frontier = new ArrayList<>();
            ArrayList<node> checked = new ArrayList<>();
            frontier.add(startingNode);

            long start = System.currentTimeMillis();
            long elapsedTime = 0;

            while (bestScore < target){
                for (int i = 0; i < nodes.size(); i++){
                    if (!frontier.contains(nodes.get(i)) && isNeighbour(currentNode, nodes.get(i))
                            && !checked.contains(nodes.get(i))){
                        frontier.add(nodes.get(i));
                    }
                }
                double maxScore = frontier.get(0).getScore();
                node maxNode = frontier.get(0);
                for (int j = 0; j < frontier.size(); j++){
                    if (frontier.get(j).getScore() == 0){
                        String newQuery = makeQuery(frontier.get(j));
                        ResultSet newRs = st.executeQuery(newQuery);
                        double newScore = 0;
                        while ( newRs.next() ) {
                            newScore = newRs.getInt("score");
                        }
                        //double newScore = newRs.getInt("score");
                        if (newScore < 0) {
                            frontier.get(j).setScore(2*newScore);
                        } else {
                            frontier.get(j).setScore(newScore);
                        }
                    }
                    if (frontier.get(j).getScore() > maxScore && !(frontier.get(j) == currentNode)){
                        maxScore = frontier.get(j).getScore();
                        maxNode = frontier.get(j);
                    }
                }
                if (maxScore > bestScore){
                    bestNode = maxNode;
                    bestScore = maxScore;

                }
                checked.add(currentNode);
                frontier.remove(currentNode);
                currentNode = maxNode;
                System.out.println("maxNode is " + maxNode.toString() + " with score of " + maxScore );
                System.out.println("bestNode is " + bestNode.toString() + " with score of " + bestScore);
                elapsedTime = System.currentTimeMillis() - start;
                System.out.println("elapsed time is: " + elapsedTime);
            }

            st.close();
            //return bestNode.toString();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return bestNode.toString();
    }

    public variable getVar1() {
        return var1;
    }

    public variable getVar2() {
        return var2;
    }

    public variable getVar3() {
        return var3;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getStr1() { return str1; }

    public double getStr2() { return str2; }

    public double getStr3() { return str3; }

    @Override
    public String toString() {
        return "node{" +
                "var1=" + var1.getName() +
                ", var2=" + var2.getName() +
                ", var3=" + var3.getName() +
                ", score=" + score +
                '}';
    }

    public static void main(String[] args) {

        //variable revGrowth = new variable("t1.revenue", -70, "desc", "(100*((t1.revenue - t0.revenue)/t0.revenue))");
        //variable earnGrowth = new variable("t1.ebit", -91,"desc","(100*((t1.ebit - t0.ebit)/t0.ebit))");
        //variable gp = new variable("t1.gp", 216,"desc","t1.gp");
        //variable ebt = new variable("t1.ebt",350 ,"desc","t1.ebt");
        //variable eps = new variable("t1.eps", 170,"desc","t1.eps");
        //variable ebit = new variable("t1.ebit", 277,"desc","t1.ebit");
        //variable netmargin = new variable("t1.netmargin", 27,"desc","t1.netmargin");
        //variable roa = new variable("t2.roa", -265,"desc","t2.roa");
        //variable roe = new variable("t2.roe", -446,"desc","t2.roe");
        //variable roic = new variable("t2.roic", -108,"desc","t2.roic");
        //variable fcf = new variable("t1.fcf", 173,"desc","t1.fcf");
        //variable de = new variable("t1.liabilities/t1.equity",191 ,"asc","t1.liabilities/t1.equity");
        //variable da = new variable("t1.liabilities/t1.assets", 292,"asc","t1.liabilities/t1.assets");
        //variable df = new variable("t1.liabilities/t1.fcf",-121 ,"asc","t1.liabilities/t1.fcf");
        //variable dof = new variable("t1.liabilities/t1.ncfo", -235,"asc","t1.liabilities/t1.ncfo");
        //variable cr = new variable("t1.currentratio", -27,"desc","t1.currentratio");

        variable revGrowth = new variable("t1.revenue", 0, "desc", "(100*((t1.revenue - t0.revenue)/t0.revenue))");
        variable earnGrowth = new variable("t1.ebit", 0,"desc","(100*((t1.ebit - t0.ebit)/t0.ebit))");
        variable gp = new variable("t1.gp", 0,"desc","t1.gp");
        variable ebt = new variable("t1.ebt",0 ,"desc","t1.ebt");
        variable eps = new variable("t1.eps", 0,"desc","t1.eps");
        variable ebit = new variable("t1.ebit", 0,"desc","t1.ebit");
        variable netmargin = new variable("t1.netmargin", 0,"desc","t1.netmargin");
        variable roa = new variable("t2.roa", 0,"desc","t2.roa");
        variable roe = new variable("t2.roe", 0,"desc","t2.roe");
        variable roic = new variable("t2.roic", 0,"desc","t2.roic");
        variable fcf = new variable("t1.fcf", 0,"desc","t1.fcf");
        variable de = new variable("t1.liabilities/t1.equity",0,"asc","t1.liabilities/t1.equity");
        variable da = new variable("t1.liabilities/t1.assets", 0,"asc","t1.liabilities/t1.assets");
        variable df = new variable("t1.liabilities/t1.fcf",0,"asc","t1.liabilities/t1.fcf");
        variable dof = new variable("t1.liabilities/t1.ncfo", 0,"asc","t1.liabilities/t1.ncfo");
        variable cr = new variable("t1.currentratio", 0,"desc","t1.currentratio");

        ArrayList<variable> variables = new ArrayList<>();
        variables.add(revGrowth); variables.add(earnGrowth); variables.add(gp); variables.add(ebt);
        variables.add(eps); variables.add(ebit); variables.add(netmargin); variables.add(roa);
        variables.add(roe); variables.add(roic); variables.add(fcf); variables.add(de);
        variables.add(da); variables.add(df); variables.add(dof); variables.add(cr);

        ArrayList<node> nodes = makeNodes(variables);
        node start = nodes.get(0);

        findBestNode(start, nodes, 3000);
    }
}

