import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';
import '../imports/templates/screener.html'
import '../imports/templates/backtester.html'
import '../imports/templates/portfolio.html'
import '../imports/templates/discovery.html'
import '../imports/templates/other.html'

import '../imports/ui/screener.js'
import '../imports/ui/backtester.js'



//Router gives path when a href is clicked
Router.map(function(){
    this.route('home', {path: '/'});
    this.route('screener', {path: '/screener'});
    this.route('backtester', {path: '/backtester'});
    this.route('portfolio', {path: '/portfolio'});
    this.route('discovery', {path: '/discovery'});
    this.route('definitions', {path: '/definitions'});
});









