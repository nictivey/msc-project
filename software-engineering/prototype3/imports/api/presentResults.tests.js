import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'chai';

if (Meteor.isServer) {
    describe('methods', () => {

        it('can get screener results', () => {
            // Find the internal implementation of the task method so we can
            // test it in isolation
            const getScreenResults = Meteor.server.method_handlers['screen'];

            // Set up a fake method invocation that looks like what the method expects
            //const invocation = { userId };

            // Run the method with `this` set to the fake invocation
            const result1 = getScreenResults.apply(null, [['assets(1)', '>', '50000'], 'eps(1)' , 'desc']);
            const result2 = getScreenResults([['assets(1)', '>', '50000'], 'eps(1)' , 'desc']);

            // Verify that the method does what we expected
            //assert.equal(result1.length, 4286);
            assert.equal(result2.length, 4286);
        });
    });
}