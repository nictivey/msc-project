import { Meteor } from 'meteor/meteor';

Meteor.methods({
    screen: function (rules, ordering, direction) {

        return new Promise(function(resolve, reject) {

            let mysql = require('mysql');
            let connection = mysql.createConnection({
                host: 'localhost',
                user: 'root',
                password: 'Smokey123$',
                database: 'market_data'
            });

            connection.connect(function (err) {
                if (err) {
                    return console.error('error: ' + err.message);
                }
                console.log('Connected to the MySQL server.');
            });

            var queryRules = "";
            var i = 0;
            while (i < rules.length / 3){
                queryRules += "AND t0." + rules[3*i] + " " + rules[3*i + 1] + " " + rules[3*i + 2] + " ";
                i++;
            }

            if (ordering === "aggregation") {
                connection.query("SELECT t0.ticker, t1.name, t0.price, t0.marketcap " +
                    "FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1, " +
                    "market_data.Fundamentals AS t2, market_data.Fundamentals AS t3 " +
                    "WHERE t0.price IS NOT null " +
                    "AND t0.calendardate = '2019-03-31' " +
                    "AND t2.calendardate = '2019-03-31' " +
                    "AND t3.calendardate = '2009-03-31' " +
                    queryRules +
                    "AND t0.eps IS NOT NULL " +
                    "AND t0.eps > 0 " +
                    "AND t0.revenue IS NOT NULL " +
                    "AND t0.revenue > 0 " +
                    "AND t2.roic IS NOT NULL " +
                    "AND t2.roic > 0 " +
                    "AND t0.dimension = 'ARQ' " +
                    "AND t2.dimension = 'ART' " +
                    "AND t3.dimension = 'ARQ' " +
                    "AND t0.permaticker = t1.permaticker " +
                    "AND t0.permaticker = t2.permaticker " +
                    "AND t0.permaticker = t3.permaticker " +
                    "ORDER BY ((1 + (170/-8))*(RANK () OVER ( ORDER BY t0.eps DESC)) + " +
                    "(1 + (-108/-8))*(RANK () OVER ( ORDER BY t2.roic DESC)) + " +
                    "(1 + (-70/-8))*(RANK () OVER ( ORDER BY (((t0.revenue - t3.revenue)/t3.revenue)*100) DESC))) " + direction + ";",
                    function (err, rows) {
                        if (err) {
                            reject(new Error("Error rows is undefined"));
                            return console.error('error: ' + err.message);
                        } else {
                            var resultArray = Object.values(JSON.parse(JSON.stringify(rows)))
                            console.log(resultArray);
                            resolve(resultArray);
                        }
                    })
            }
            else {
                connection.query("SELECT t0.ticker, t1.name, t0.price, t0.marketcap " +
                    "FROM market_data.Fundamentals AS t0, market_data.Tickers AS t1 " +
                    "WHERE t0.price IS NOT null " +
                    "AND t0.calendardate = '2019-03-31' " +
                    queryRules +
                    "AND t0." + ordering + " IS NOT NULL " +
                    "AND t0.dimension = 'ARQ' " +
                    "AND t0.permaticker = t1.permaticker " +
                    "ORDER BY t0." + ordering + " " + direction + ";", function (err, rows) {
                    if (err) {
                        reject(new Error("Error rows is undefined"));
                        return console.error('error: ' + err.message);
                    } else {
                        var resultArray = Object.values(JSON.parse(JSON.stringify(rows)))
                        console.log(resultArray);

                        resolve(resultArray);
                    }
                })
            }
            connection.end();
        });
    },

    backtest: function () {

    }
});
