//Method to delete the initial rule
import {Template} from "meteor/templating";

import '../api/presentResults.js'

//List of all variables
var optionList =["open(1)", "high(1)", "low(1)", "close(1)", "volume(1)", "dividends(1)", "closeunadj(1)",
    "accoci(1)", "assets(1)", "assetsavg(1)", "assetsc(1)", "assetsnc(1)",
    "assetturnover(1)", "bvps(1)", "capex(1)", "cashneq(1)", "cashnequsd(1)", "cor(1)",
    "consolinc(1)", "currentratio(1)", "de(1)", "debt(1)", "debtc(1)", "debtnc(1)",
    "debtusd(1)", "deferredrev(1)", "depamor(1)", "deposits(1)", "divyeild(1)", "dps(1)",
    "ebit(1)", "ebitda(1)", "ebitdamargin(1)", "ebitdausd(1)", "ebitusd(1)", "ebt(1)",
    "eps(1)", "epsdil(1)", "epsusd(1)", "equity(1)", "equityavg(1)", "equityusd(1)", "ev(1)",
    "evebit(1)", "evebitda(1)", "fcf(1)", "fcfps(1)", "fxusd(1)", "gp(1)", "grossmargin(1)",
    "intangibles(1)", "intexp(1)", "invcap(1)", "invcapavg(1)", "inventory(1)", "investments(1)",
    "investmentsc(1)", "investmentsnc(1)", "liabilities(1)", "liabilitiesc(1)", "liabilitiesnc(1)",
    "marketcap(1)", "ncf(1)", "ncfbus(1)", "ncfcommon(1)", "ncfdebt(1)", "ncfdiv(1)", "ncff(1)",
    "ncfi(1)", "ncfinv(1)", "ncfo(1)", "ncfx(1)", "netinc(1)", "netinccmn(1)", "netinccmnusd(1)",
    "netincdis(1)", "netincnci(1)", "netmargin(1)", "opex(1)", "opinc(1)", "payables(1)",
    "payoutratio(1)", "pb(1)", "pe(1)", "pe1(1)", "ppnenet(1)", "prefdivis(1)", "price(1)", "ps(1)",
    "ps1(1)", "recievables(1)", "retearn(1)", "revenue(1)", "revenueusd(1)", "rnd(1)", "roa(1)",
    "roe(1)", "roic(1)", "ros(1)", "sbcomp(1)", "sgna(1)", "sharefctor(1)", "sharesbas(1)",
    "shareswa(1)", "shareswadil(1)", "sps(1)", "tangibles(1)", "taxassets(1)", "taxexp(1)",
    "taxliabilities(1)", "tbvps(1)", "workingcapital(1)"];

//List of all operators
var operatorList =[">", "<", "=", "!=", ">=", "<="];

//Function that adds a rule, can be given inputs that determine value for each entry.
function addToRules(v1, v2, v3){

    //clears any error message
    document.getElementById('errors').innerHTML =
        "";

    var variable = document.createElement('input');
    var value = document.createElement('input');
    var operator = document.createElement('select');
    variable.id = 'var';
    variable.value = v1;
    value.id = 'val';
    value.value = v3;
    operator.id = 'op';



    var dl1 = document.createElement('datalist');
    dl1.id = 'variables';
    var dl2 = document.createElement('datalist');
    dl2.id = 'values';

    variable.setAttribute('list','variables');
    value.setAttribute('list', 'values');

    var len = optionList.length;
    for (var i=0; i < len; i++) {
        var option = document.createElement('option');
        option.value = optionList[i];
        if (i < 7) {
            option.label = "technical";
        } else {
            option.label = "fundamental";
            dl1.appendChild(option);
        }
        //dl1.appendChild(option);
    }

    variable.appendChild(dl1);

    for (var i=0; i < len; i++) {
        var option = document.createElement('option');
        option.value = optionList[i];
        if (i < 7) {
            option.label = "technical";
        } else {
            option.label = "fundamental";
            dl2.appendChild(option);
        }
        //dl2.appendChild(option);
    }

    value.appendChild(dl2);

    var len2 = operatorList.length;
    for (var i = 0; i < len2; i++){
        var option = document.createElement('option');
        option.text = operatorList[i];

        if (operatorList[i] == v2) {
            option.selected = "selected";
        }
        if (operatorList[i] == "<"){
            option.value = "lt";
            operator.appendChild(option);
        } else {
            option.value = operatorList[i];
            operator.appendChild(option)
        }
    }

    //creating a delete button that removes the li item it is within when clicked
    var deleteButton = document.createElement('button');
    deleteButton.innerHTML = '&times';
    deleteButton.onclick = function(){
        if (this.parentNode.parentNode.children.length > 1) {
            this.parentNode.parentNode.removeChild(this.parentNode);
        } else {
            document.getElementById('errors').innerHTML =
                "<b> Cannot Delete Final Rule </b>";
        }
        return false;
    }

    const newnode = document.createElement("LI");
    newnode.append(variable, "  ", operator, "  ", value, "  ", deleteButton);
    document.getElementById("rules").appendChild(newnode);
}

function checkValidityRules(v1, v2, v3, v4, v5){

    const v1trimmed = v1.trim();
    const v2trimmed = v2.trim();

    if (v1trimmed === ""){ // || optionList.indexOf(v1 + "") === -1) {
        v3[v4].getElementsByTagName('input').item(0).dataset.state = 'invalid';
        v5 = false;
    } else {
        v3[v4].getElementsByTagName('input').item(0).dataset.state = '';
    }

    if (v2trimmed === ""){ // || optionList.indexOf(v2 + "") === -1) {
        v3[v4].getElementsByTagName('input').item(1).dataset.state = 'invalid';
        v5 = false;
    } else {
        v3[v4].getElementsByTagName('input').item(1).dataset.state = '';
    }
    return v5;
}

function checkValidityScreener(v1, v2, v3){

    const v1trimmed = v1.trim();
    const v2trimmed = v2.trim();

    if (v1trimmed === ""){
        document.getElementById('rankingInput').dataset.state = 'invalid';
        v3 = false;
    } else {
        document.getElementById('rankingInput').dataset.state = '';
    }

    if (v2trimmed === ""){
        event.target.quantity.dataset.state = 'invalid';
        v3 = false;
    } else {
        event.target.quantity.dataset.state = '';
    }

    return v3;
}

Template.screener.events({
    'click .initialDelete': function (event) {

        event.preventDefault();
        var rules = document.getElementById('rules');
        if (rules.children.length > 1)
        {rules.removeChild(rules.getElementsByTagName('li').item(0));}
        else {
            document.getElementById('errors').innerHTML = "<b>Cannot Delete Final Rule</b>";
        }
    }
})

//event that removes current rules and replaces them with the rules used in the Tiny Titans Strategy.
Template.sidebar.events({
    'click .tinytitans': function (event) {
        event.preventDefault();
        document.getElementById('rules').innerHTML = "";
        addToRules('price(1)', '>', 'price(5)')
        addToRules('ps(1)', '<', '1');
        addToRules('invcap(1)', '>=', '500000');
        addToRules('marketcap(1)', '>', '25000000');
        addToRules('marketcap(1)', '<', '250000000');
    }
});

//event that removes current rules and replaces them with the rules used in the Zulu principle Strategy.
Template.sidebar.events({
    'click .zulu': function (event) {
        event.preventDefault();
        document.getElementById('rules').innerHTML = "";
        addToRules('price(1) / (100 * ((eps(1) - eps(5)) / eps(5)))', '<', '0.75')
        addToRules('price(1)', '>', 'price(5)')
        addToRules('pe(1)', '<', '20');
        addToRules('pe(2)', '<', '20');
        addToRules('pe(3)', '<', '20');
        addToRules('pe(4)', '<', '20');
        addToRules('100 * ((eps(1) - eps(5))/ eps(5))', '>', '15')
        addToRules('roic(1)', '>', '12');
        addToRules('marketcap(1)', '>', '20000000');
        addToRules('marketcap(1)', '<', '1000000000');
    }
});

//Method that adds a rule with empty values
Template.screener.events({
    'click .add': function (event) {
        event.preventDefault();
        addToRules("", "", "");
    }
});

Template.screener.events({
    'submit form': function (event) {
        event.preventDefault();

        var valid1 = true;
        var valid2 = true;

        var list = document.getElementById('rules').children;
        var len = list.length;
        //var result = "screener/";
        let rules = [];
        for (var i = 0; i < len; i++) {
            var variable = list[i].getElementsByTagName('input').item(0).value;
            var operator = list[i].getElementsByTagName('select').item(0).value;
            var value = list[i].getElementsByTagName('input').item(1).value;
            //result += variable + "/" + operator + "/" + value + "/";
            valid1 = checkValidityRules(variable, value, list, i, valid1);
            var acVariable = variable.substring(0, variable.length - 3);
            if (operator === "lt"){
                operator = "<"
            }

            rules[3*i] = acVariable;
            rules[3*i + 1] = operator;
            rules[3*i + 2] = value;
        }

        var orderBy = document.getElementById('rankingInput').value;
        var direction = event.target.rank.value;
        var quantity = event.target.quantity.value;
        //result += orderBy + "/" + direction + "/" + quantity;

        var ordering;
        valid2 = checkValidityScreener(orderBy, quantity, valid2);
        if (orderBy !== "aggregation") {
            ordering = orderBy.substring(0, orderBy.length - 3);
        } else {
            ordering = orderBy;
        }

        if (valid1 && valid2) {

            document.getElementById('errors').innerHTML = "";
            document.getElementById('results').innerHTML = "<h2>Results - <small>Processing</small></h2>" +
                "<div class='loader'></div>";
            Meteor.call('screen', rules, ordering, direction, function(error, result1){
                if(error){
                    alert('Error');
                }else{
                    document.getElementById('mySidebar').style.width = '0px';
                    document.getElementById('main').style.marginRight= '0';
                    document.getElementById('main2').style.marginRight= '0';
                    var data;
                    if (result1.length === 0){
                        data = "<h2>Results</h2><p>Showing 0 of 0 results</p>";
                        quantity = 0;
                    } else if (result1.length < quantity){
                        data = "<h2>Results</h2><p>Showing " + result1.length + " of " + result1.length + " results"
                        quantity = result1.length;
                    } else {
                        data = "<h2>Results</h2>" +
                            "<p>Showing " + quantity + " of " + result1.length + " results</p>";
                    }
                    data += "<div style='overflow-x:auto;'>" +
                       "<table width='100' border='1'>" +
                       "<thead>" +
                       "<tr>" +
                        "<th>No</th>" +
                       "<th>ticker</th>" +
                       "<th>name</th>" +
                       "<th>price(1)</th>" +
                       "<th>marketcap(1)</th>" +
                       "<th>Portfolio</th>" +
                       "</tr>" +
                       "</thead><tbody>";
                        var i = 1;
                        quantity++;
                        while (i < quantity){
                            data +="<tr><td>" + i + "</td><td>" + result1[i-1].ticker + "</td><td>" +
                                 result1[i-1].name + "</td><td>" + result1[i-1].price + "</td><td>" +
                                 result1[i-1].marketcap + "</td><td align='center'><input class='button1' type='button' value='Add'></td></tr>";
                            i++;
                    }
                    data += "</tbody></table></div>";
                    document.getElementById('results').innerHTML = data;
                    i = 1;
                }
            });
        } else {
            document.getElementById('errors').innerHTML =
                "<b> Fix Errors </b>";
        }
    }
});

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

Template.screener.events({
    'click .save': function(event) {
        event.preventDefault();
        var id = makeid(5);
        var valid = true;

        var variables = [];
        var operators = [];
        var values = [];

        var list = document.getElementById('rules').children;
        var len = list.length;
        var i = 0;
        while (i < len){
            var variable = list[i].getElementsByTagName('input').item(0).value;
            var operator = list[i].getElementsByTagName('select').item(0).value;
            var value = list[i].getElementsByTagName('input').item(1).value;

            valid = checkValidityRules(variable, value, list, i, valid);

            variables[i] = variable;
            operators[i] = operator;
            values[i] = value;

            i++;
        }
        i = 0;

        if (valid) {
            document.getElementById('errors').innerHTML = "";

            const newnode = document.createElement("LI");
            const ref = document.createElement("a");
            ref.innerHTML = id;
            ref.href = "#";
            ref.style = "color: green";
            ref.onclick = function () {
                document.getElementById("rules").innerHTML = "";
                while (i < len) {
                    addToRules(variables[i], operators[i], values[i]);
                    i++;
                }
                i = 0;
            }
            var deleteButton = document.createElement('button');
            deleteButton.innerHTML = '&times';
            deleteButton.style = "float: right";
            deleteButton.onclick = function () {
                this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
            }
            ref.appendChild(deleteButton);
            newnode.appendChild(ref);
            document.getElementById("saved").appendChild(newnode);
        } else {
            document.getElementById('errors').innerHTML =
                "<b> Must have complete rules </b>";
        }
    }
});


